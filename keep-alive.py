import requests
import argparse
import time


def parse_args():
    arg_p = argparse.ArgumentParser(
        description="keep sign-in in unipd elearning platform"
    )

    browsers = ["chrome", "firefox", "safari", "none"]
    arg_p.add_argument(
        "browser",
        choices=browsers,
        help="Browsers, `none' if you want to enter cookie directly",
    )
    arg_p.add_argument("--cookie", help="In case of using `none' as the browser")
    arg_p.add_argument(
        "--interval", type=int, default=30, help="Refresh interval in minute"
    )
    args = arg_p.parse_args()
    return args


def get_browser_cookies(browser_name):
    try:
        cj = getattr(browsercookie, browser_name)()
    except:
        print("Can't load cookies from this browser. See --cookie option")
        exit(1)

    cookies = {}
    for c in cj:
        if c.domain == "elearning.dei.unipd.it":
            cookies[c.name] = c.value
    return cookies


def logged_in(html):
    return 'class="userbutton"' in html


def loop(args, cookies):
    delay = args.interval * 60
    while True:
        res = requests.get("https://elearning.dei.unipd.it", cookies=cookies)
        if not logged_in(res.text):
            if args.browser == "none":
                print(
                    "The Cookie has been expired, "
                    "re-run the script with a new cookie"
                )
                exit(0)
            else:
                print("Please login with your selected browser")
                time.sleep(5 * 60)
                cookies = get_browser_cookies(args.browser)
                continue
        print(f"Refresh successfully, sleeping for {args.interval} minutes")
        time.sleep(delay)


if __name__ == "__main__":
    args = parse_args()
    if args.browser != "none":
        try:
            import browsercookie
        except ImportError:
            msg = "browsercookie module is needed to get the cookies "
            "from the browser"
            print(msg)
        cookies = get_browser_cookies(args.browser)
    else:
        cookies = {"MoodleSession": args.cookie}

    try:
        loop(args, cookies)
    except KeyboardInterrupt:
        print("Exit")
        exit(0)
