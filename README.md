# What is this for

I'm a student in the university of Padova. In our Moodle platform, the session
cookie expires very soon if you don't have activity for a while, so you need to
sign in very often which is pretty annoying for me!
Because, there is no "Remember me" option when you're signing in, I decided
to write a simple script to refresh the page behalf of you in order to renew
your session cookie.

# How to use it

When you want to start the script, you can specify what web browser you're
using. The script tries to extract neccesary cookies from your browser's
database and send a simple `HTTP GET` request with your cookie session.
You can pick one of these browsers: firefox, chrome or safari.

The first positional argument is the name of your browser. For example, if
you're using firefox:

```
python keep-alive.py firefox
```

Or for chrome:

```
python keep-alive.py chrome
```

But don't worry if you're using unsupported browser or you don't want to allow
the script to read your cookie database. There is a special feature that
you can enter your Moodle's session cookie manualy which is my prefer option,
although I often use firefox or chromium. Maybe you wonder why?

I have some reasons why I prefer to enter cookie manualy:

* In order to extract cookies from browser's database, you need to install
special library called `browsercookie`. To be honest, It looks a bit awkard to
install a library just for this simple purpose.

* Or maybe you want to run this script in your cloud instead of running it on
your machine. This way it can keep runnig forever and your session never be
expired. Just remember to run the script inside Tmux or Screen, hence it won't
be killed even if you close your remote shell session.

* You can run multiple instances of the script for different sessions.
For example, if you've signed in from your laptop and cellphone and
you want to keep both of them alive.

To enter the cookie manually, run the script like this:

```
python keep-alive.py none --cookie YOUR-MOODLE-SESSION-COOKIE
```

* `none` means you're not using any browser
* Replace `YOUR-MOODLE-SESSION-COOKIE` with your actual cookie extracted
from your browser. You know how to do it right? (Hint: using developer tools)
